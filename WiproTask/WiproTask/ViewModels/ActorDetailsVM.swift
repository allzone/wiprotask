//
//  ActorDetailsVM.swift
//  WiproTask
//
//  Created by allzone on 12/9/17.
//  Copyright © 2017 Ion Brumaru. All rights reserved.
//

import Foundation

class ActorDetailsVM: NSObject {
    
    var selectedItem : Actor!
    
    init(actor: Actor) {
        super.init()
        self.selectedItem = actor
    }
}
