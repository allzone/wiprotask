//
//  ActorsListVM.swift
//  WiproTask
//
//  Created by allzone on 12/9/17.
//  Copyright © 2017 Ion Brumaru. All rights reserved.
//

import Foundation
import UIKit

// MARK: Constants

extension ActorsListVM {
    static let defaultCellId = "ActorCell"
    static let detailsSegue = "ActorDetails"
}

class ActorsListVM: NSObject {
    
    var reloadTableViewCallback : (()->())!
    var dataArray = [Actor]()
    var selectedCellIndexPath : IndexPath!
    
    init(reloadTableViewCallback : @escaping (()->())) {
        super.init()
        
        self.reloadTableViewCallback = reloadTableViewCallback
        retrieveData()
    }
    
    //invoke data manager to get new data
    func retrieveData() {
        Network.manager.getActorsList(path: Network.Paths.actorsList) { list in
            self.dataArray = list
            self.reloadTableViewCallback()
        }
    }
    
    func numberOfItemsInSection(section : Int) -> Int {
        return dataArray.count
    }

    func setUpTableviewViewCell(indexPath : IndexPath, tableView : UITableView) -> UITableViewCell {
        
        let actorItem = dataArray[indexPath.item]
        
        let cell = tableView.dequeueReusableCell(withIdentifier: ActorsListVM.defaultCellId, for: indexPath) as! ActorListCell
        cell.setupData(model: actorItem)
        
        return cell
    }
    
    func setSelectedCellIndexPath(indexPath : IndexPath){
        selectedCellIndexPath = indexPath
    }
    
    func selectedItem() -> Actor? {
        guard let indexPath = selectedCellIndexPath else {
            return nil
        }
        
        return dataArray[indexPath.row]
    }
}
