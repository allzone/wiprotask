//
//  UIImageView.swift
//  WiproTask
//
//  Created by allzone on 12/9/17.
//  Copyright © 2017 Ion Brumaru. All rights reserved.
//

import UIKit

extension UIImageView {
    func loadFrom(url: URL) {
        URLSession.shared.dataTask(with: url) { (data, response, error) in
            guard
                let httpURLResponse = response as? HTTPURLResponse, httpURLResponse.statusCode == 200,
                let mimeType = response?.mimeType, mimeType.hasPrefix("image"),
                let data = data, error == nil,
                let image = UIImage(data: data)
                else { return }
            DispatchQueue.main.async() { () -> Void in
                self.image = image
            }
            }.resume()
    }
}
