//
//  UIViewController.swift
//  WiproTask
//
//  Created by allzone on 12/9/17.
//  Copyright © 2017 Ion Brumaru. All rights reserved.
//

import Foundation
import MBProgressHUD

extension UIViewController {
    // MARK: HUD indicator utils
    func showHUD(animated: Bool = true) {
        MBProgressHUD.showAdded(to: view, animated: animated)
    }
    
    func hideHUD(animated: Bool = true) {
        MBProgressHUD.hide(for: view, animated: animated)
    }
}
