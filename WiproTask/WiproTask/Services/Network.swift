//
//  Network.swift
//  WiproTask
//
//  Created by allzone on 12/9/17.
//  Copyright © 2017 Ion Brumaru. All rights reserved.
//

import Foundation
import Alamofire

extension Network {
    struct Paths {
        fileprivate static let baseUrl = "http://microblogging.wingnity.com/"
        static let actorsList = "\(Paths.baseUrl)JSONParsingTutorial/jsonActors"
    }
}

struct Network {
    
    static let manager = Network()
    
    func getActorsList(path: String, completion: @escaping (_ actors: [Actor]) -> Void) {
        Alamofire.request(path).responseJSON { response in
            if let dict = response.result.value as? NSDictionary, let list = dict["actors"] as? [NSDictionary] {
                let actors = list.flatMap { Actor.from($0) }
                completion(actors)
            }
        }
    }
}

