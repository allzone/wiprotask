//
//  ActorListCell.swift
//  WiproTask
//
//  Created by allzone on 12/9/17.
//  Copyright © 2017 Ion Brumaru. All rights reserved.
//

import UIKit

class ActorListCell: UITableViewCell {
    @IBOutlet weak var actorName: UILabel!
    @IBOutlet weak var actorDescription: UILabel!
    
    @IBOutlet weak var actorImage: UIImageView!
    
    func setupData(model: Actor){
        actorName.text = model.fullName
        actorDescription.text = model.description
        
        if let avatarUrl = URL(string: model.avatar) {
            actorImage.loadFrom(url: avatarUrl)
        }
    }
}
