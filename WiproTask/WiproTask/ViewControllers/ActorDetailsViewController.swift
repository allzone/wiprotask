//
//  ActorDetailsViewController.swift
//  WiproTask
//
//  Created by allzone on 12/9/17.
//  Copyright © 2017 Ion Brumaru. All rights reserved.
//

import UIKit

class ActorDetailsViewController: UIViewController {
    
    @IBOutlet weak var actorName: UILabel!
    @IBOutlet weak var actorCountry: UILabel!
    @IBOutlet weak var actorBirth: UILabel!
    @IBOutlet weak var actorHeight: UILabel!
    @IBOutlet weak var actorSpouse: UILabel!
    @IBOutlet weak var actorChildren: UILabel!
    @IBOutlet weak var actorDescription: UILabel!
    
    @IBOutlet weak var actorAvatar: UIImageView!
    
    public var viewModel: ActorDetailsVM!

    override func viewDidLoad() {
        super.viewDidLoad()
        fillScreenWithInfo()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = false
    }
    
    fileprivate func fillScreenWithInfo() {
        actorName.text = viewModel.selectedItem.fullName
        actorBirth.text = "Birth: \(viewModel.selectedItem.birthDate)"
        actorCountry.text = "Country: \(viewModel.selectedItem.country)"
        actorHeight.text = "Height: \(viewModel.selectedItem.height)"
        actorSpouse.text = "Spouse: \(viewModel.selectedItem.spouse)"
        actorChildren.text = "Children: \(viewModel.selectedItem.children)"
        actorDescription.text = viewModel.selectedItem.description
        
        if let url = URL(string: viewModel.selectedItem.avatar) {
            actorAvatar.loadFrom(url: url)
        }
    }
}
