//
//  ActorListViewController.swift
//  WiproTask
//
//  Created by allzone on 12/9/17.
//  Copyright © 2017 Ion Brumaru. All rights reserved.
//

import UIKit

class ActorListViewController: UIViewController {
    
    @IBOutlet weak internal var tableView: UITableView!
    
    public var viewModel: ActorsListVM!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        showHUD()
        viewModel = ActorsListVM(reloadTableViewCallback: reloadTableViewData)
        tableView.rowHeight = UITableViewAutomaticDimension
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        navigationController?.navigationBar.isHidden = true
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == ActorsListVM.detailsSegue {
            let vc = segue.destination as! ActorDetailsViewController
            setupActorDetails(vc: vc)
        }
    }
    
    // refresh list
    func reloadTableViewData(){
        tableView.reloadData()
        hideHUD()
    }
    
    fileprivate func showActorDetails() {
        performSegue(withIdentifier: ActorsListVM.detailsSegue, sender: self)
    }

    private func setupActorDetails(vc: ActorDetailsViewController) {
        guard let item = viewModel.selectedItem() else {
            return
        }
        vc.viewModel = ActorDetailsVM(actor: item)
    }
}

// MARK: UITableViewDataSource
extension ActorListViewController: UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModel.numberOfItemsInSection(section: section)
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        return viewModel.setUpTableviewViewCell(indexPath: indexPath, tableView: tableView)
    }
}

// MARK: UITableViewDelegate
extension ActorListViewController: UITableViewDelegate {
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
        viewModel.setSelectedCellIndexPath(indexPath: indexPath)
        showActorDetails()
    }
}
