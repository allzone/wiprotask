//
//  ActorModel.swift
//  WiproTask
//
//  Created by allzone on 12/9/17.
//  Copyright © 2017 Ion Brumaru. All rights reserved.
//

import Foundation
import Mapper

public struct Actor {
    let fullName: String
    let description: String
    let birthDate: String
    let country: String
    let height: String
    let spouse: String
    let children: String
    let avatar: String
}

// MARK: Parsing

extension Actor: Mappable {
    
    struct Keys {
        static let fullName = "name"
        static let description = "description"
        static let birthDate = "dob"
        static let country = "country"
        static let height = "height"
        static let spouse = "spouse"
        static let children = "children"
        static let avatar = "image"
    }
    
    public init(map: Mapper) throws {
        try fullName = map.from(Keys.fullName)
        try description = map.from(Keys.description)
        try birthDate = map.from(Keys.birthDate)
        try country = map.from(Keys.country)
        try height = map.from(Keys.height)
        try spouse = map.from(Keys.spouse)
        try children = map.from(Keys.children)
        try avatar = map.from(Keys.avatar)
    }
}
