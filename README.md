# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

Assignment 1:

Create An Swift app which Parsh the below URL and display result in Tableview formate with custom cell like below
http://microblogging.wingnity.com/JSONParsingTutorial/jsonActors
Sample GUI – Screen Shot 2017 ( Attached)

Note: Use Swift based feature like "Promis Kit" , "Alamofire"  , "Object Mapper"  etc to create your app.


## Get started
The project is built using:

- Swift 4
- Xcode 9.1
- Cocoapods

## Features
- [x] Actors list
- [x] Parse json file
- [x] Actor list details
- [ ] Unit tests

